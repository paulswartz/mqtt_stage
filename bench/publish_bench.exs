config = EmqttFailover.Config.from_url("mqtt://system:manager@localhost")
topic = "emqtt_failover/bench/#{System.unique_integer()}"

Benchee.run(
  %{
    "publish" => fn {messages, client} ->
      for m <- messages do
        :ok = EmqttFailover.Connection.publish(client, m)
      end
    end
  },
  time: 5,
  parallel: 4,
  inputs: %{
    # all qos: 0 (disabled because it makes the local ActiveMQ run out of memory)
    # "0" => {1, 0, 8},
    # all qos: 1
    "1" => {0, 1, 8},
    # alternate qos: 0 and qos: 1
    "0_1" => {1, 1, 4},
    # 3 qos: 0 for each qos: 1
    "0_0_0_1" => {3, 1, 2},
    # 7 qos: 0 for each qos: 1
    "0_0_0_0_0_0_0_1" => {7, 1, 1}
  },
  before_scenario: fn qos_count ->
    {:ok, client} =
      EmqttFailover.Connection.start_link(
        configs: [config],
        handler: {EmqttFailover.ConnectionHandler.Parent, parent: self()}
      )

    receive do
      {:connected, ^client} ->
        :ok
    after
      2_000 ->
        IEx.Helpers.flush()
        raise "unable to connect"
    end

    message = %EmqttFailover.Message{
      topic: topic,
      payload: "#{System.unique_integer()}"
    }

    {zero_count, one_count, loops} = qos_count

    qos_0_messages =
      for _ <- Range.new(1, zero_count, 1) do
        message
      end

    qos_1_messages =
      for _ <- Range.new(1, one_count, 1) do
        %{message | qos: 1}
      end

    messages =
      for _ <- 1..loops,
          m <- qos_0_messages ++ qos_1_messages do
        m
      end

    {messages, client}
  end
)
