# CHANGELOG

## v0.3.1

- fix: handle errors when subscribing
- fix: EmqttFailover.Connection.start_link also takes GenServer.options()

## v0.3.0

- feat: only start backing off once all configs have been tried
- feat: vendor the parts of `:emqtt` we currently use

## v0.2.1

- feat: added `EmqttFailover.Connection.publish_async/2` which does not wait for a reply
- feat: `EmqttFailover.Connection.publish/2` does not time out

## v0.2

- BREAKING: new callback `handle_disconnected` for `EmqttFailover.ConnectionHandler`
- `EmqttFailover.ConnectionHandler.Parent` which sends updates to a parent process. This extracts a common pattern from users of the library.
- allow setting the `client_id` for `EmqttFailover.Connection`
- better handling of unexpected messages in `EmqttFailover.Connection`

## v0.1.1

- don't lock `EmqttFailover.Connection` during startup; prevents crashes when calling `publish` during connection

## v0.1.0

- initial release

