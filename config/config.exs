import Config

if Config.config_env() != :prod do
  config :emqtt_failover, :test_hostname, System.get_env("ACTIVEMQ_HOSTNAME", "localhost")
  # emqtt logs a lot at the debug level
  config :logger, level: :warning
end
