defmodule EmqttFailover do
  @moduledoc """
  Helper functions for supporting EmqttFailover.
  """

  @type client_id_opt :: {:prefix, binary} | {:hostname, binary}

  @doc """
  Generate a unique client ID, like `"<prefix>-<hostname>-<random suffix>"`

  Options:
  - `prefix`: defaults to `"emqtt-f"`
  - `hostname`: defaults to the value of `:inet.gethostname()`
  """
  @spec client_id() :: binary
  @spec client_id([client_id_opt]) :: binary
  def client_id(opts \\ []) do
    prefix = Keyword.get(opts, :prefix, "emqtt-f")

    hostname =
      case Keyword.fetch(opts, :hostname) do
        {:ok, value} ->
          value

        :error ->
          {:ok, hostname} = :inet.gethostname()
          hostname
      end

    suffix = Base.encode32(:crypto.strong_rand_bytes(5), case: :lower, padding: false)
    "#{prefix}-#{hostname}-#{suffix}"
  end
end
