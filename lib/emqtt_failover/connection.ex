defmodule EmqttFailover.Connection do
  @moduledoc """
  Wraps an :emqtt client.
  """
  use GenServer
  require Logger

  alias EmqttFailover.Config
  alias EmqttFailover.ConnectionHandler.Default, as: DefaultHandler
  alias EmqttFailover.Message

  @default_backoff {5_000, 300_000, :jitter}

  defstruct [
    :client,
    :client_id,
    :configs,
    :handler,
    :backoff,
    config_index: 0,
    status: :not_connected
  ]

  @type config() :: Config.t() | String.t()
  @type backoff ::
          non_neg_integer()
          | {non_neg_integer(), non_neg_integer()}
          | {non_neg_integer(), non_neg_integer(), :normal | :jitter}
  @type handler ::
          module() | {module(), term()}
  @type param() ::
          {:backoff, backoff()}
          | {:configs, Enumerable.t(config()) | config()}
          | {:client_id, binary()}
          | {:handler, handler()}
          | GenServer.option()

  @type status() :: :not_connected | :connected

  @typep state() :: %__MODULE__{
           client: pid() | nil,
           status: status(),
           configs: [Config.t()],
           config_index: non_neg_integer(),
           backoff: {timeout(), :backoff.backoff()},
           handler: {module, EmqttFailover.ConnectionHandler.state()}
         }

  @doc """
  Start a `#{__MODULE__}`.

  Required parameters:
  - `configs`: an Enumerable of URLs or `EmqttFailover.Config` structs. These will be tried in order.

  Optional parameters:

  - `backoff`: either a single number of milliseconds to wait after a connection
    failure, a tuple `{start, max}` where `start` and `max` are the initial and
    maximum number of milliseconds to wait, or `{start, max, :jitter}` to add
    some random jitter to the specific reconnection times. Note: this backoff
    timeout will only be used after all the failover connections have been tried.

  - `client_id`: a binary to be used as the identifier for this client

  - `handler`: either a module implementing `EmqttFailover.ConnectionHandler` or
    a tuple `{module, init_args}` where `init_args` are passed to the module's
    `init/1` function.

  """
  @spec start_link(config() | [param()]) :: GenServer.on_start()
  def start_link(config_or_opts) do
    init =
      case config_or_opts do
        %Config{} ->
          %{configs: [config_or_opts]}

        url when is_binary(url) ->
          %{configs: [url]}

        _ ->
          Map.new(config_or_opts)
      end

    start_link_opts =
      init
      |> Map.take([:debug, :name, :timeout, :spawn_opt, :hibernate_after])
      |> Keyword.new()

    GenServer.start_link(__MODULE__, init, start_link_opts)
  end

  @doc """
  Gracefully stops the connection.
  """
  @spec stop(GenServer.server()) :: :ok
  @spec stop(GenServer.server(), timeout()) :: :ok
  def stop(server, timeout \\ :infinity) do
    GenServer.stop(server, :normal, timeout)
  end

  @doc """
  Returns the current connection status, along with the configuration for the connection.

  If the `status` is `:not_connected`, the config that is returned is the next one that will be tried.
  """
  @spec status(GenServer.server()) :: {status(), Config.t()}
  def status(server) do
    GenServer.call(server, :status)
  end

  @doc """
  Publishes a message to the broker.

  For more on the message, see `EmqttFailover.Message`.

  For `qos: 0` messages, this always returns `:ok`

  If the message has `qos: 1` or `qos: 2` and the server is not connected or the message is not acknowledged, this will return an error.
  """
  @spec publish(GenServer.server(), Message.t()) :: :ok | {:error, term()}
  @spec publish(GenServer.server(), Message.t(), timeout) :: :ok | {:error, term()}
  def publish(server, %Message{} = message, timeout \\ :infinity) do
    GenServer.call(server, {:publish, message}, timeout)
  end

  @doc """
  Publishes a message to the broker without waiting for a reply.

  For more on the message, see `EmqttFailover.Message`.
  """
  @spec publish_async(GenServer.server(), Message.t()) :: :ok
  def publish_async(server, %Message{} = message) do
    GenServer.cast(server, {:publish_async, message})
  end

  @doc false
  def port(server) do
    GenServer.call(server, :port)
  end

  @impl GenServer
  def init(params) do
    Process.flag(:trap_exit, true)

    configs = init_configs(params.configs)

    handler = init_handler(Map.get(params, :handler))

    backoff = init_backoff(Map.get(params, :backoff) || @default_backoff)

    client_id = Map.get(params, :client_id) || EmqttFailover.client_id()

    state = %__MODULE__{
      backoff: backoff,
      client_id: client_id,
      configs: configs,
      handler: handler
    }

    {:ok, state, {:continue, :try_to_connect}}
  end

  @impl GenServer
  def handle_continue(:try_to_connect, state) do
    config = current_config(state)

    {:ok, pid} =
      config
      |> Config.to_emqtt()
      |> Keyword.put(:clientid, state.client_id)
      |> :emqtt.start_link()

    state = %{state | client: pid}

    parent = self()

    spawn(fn ->
      # trigger the connect in a separate process to avoid blocking the server
      try do
        send(parent, {:emqtt_connect, :emqtt.connect(pid)})
      catch
        :exit, reason ->
          send(parent, {:emqtt_connect, {:error, reason}})
      end
    end)

    {:noreply, state}
  end

  @impl GenServer
  def handle_call(:status, _from, state) do
    config = current_config(state)
    {:reply, {state.status, config}, state}
  end

  def handle_call({:publish, message}, _from, %{status: :connected} = state) do
    opts = [
      qos: message.qos,
      retain: message.retain?
    ]

    result =
      case :emqtt.publish(state.client, message.topic, message.payload, opts) do
        :ok ->
          :ok

        {:ok, _} ->
          :ok

        {:error, error} ->
          call_publish_error_reply(error, message)
      end

    {:reply, result, state}
  catch
    :exit, reason ->
      {:reply, call_publish_error_reply(reason, message), state}
  end

  def handle_call({:publish, message}, _from, state) do
    {:reply, call_publish_error_reply(state.status, message), state}
  end

  def handle_call(:port, _from, state) do
    # pull the TCP port out of the emqtt client state and close it
    port = Keyword.get(:emqtt.info(state.client), :socket)
    {:reply, port, state}
  end

  @impl GenServer
  def handle_cast({:publish_async, message}, %{status: :connected} = state) do
    opts = [
      qos: message.qos,
      retain: message.retain?
    ]

    :ok =
      :emqtt.publish_async(
        state.client,
        message.topic,
        message.payload,
        opts,
        :undefined
      )

    {:noreply, state}
  catch
    :exit, _reason ->
      {:noreply, state}
  end

  def handle_cast({:publish_async, _message}, state) do
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:publish, %{client_pid: client} = published}, %{client: client} = state) do
    message = %Message{
      topic: published.topic,
      payload: published.payload,
      qos: published.qos,
      retain?: published.retain
    }

    {handler, handler_state} = state.handler
    {:ok, handler_state} = handler.handle_message(message, handler_state)
    state = %{state | handler: {handler, handler_state}}
    {:noreply, state}
  end

  def handle_info({:emqtt_connect, response}, state) do
    handle_connect_response(response, state)
  end

  def handle_info(:reconnect, %{status: :not_connected} = state) do
    {:noreply, state, {:continue, :try_to_connect}}
  end

  def handle_info(:reconnect, %{status: :connected} = state) do
    # already connected, ignore
    {:noreply, state}
  end

  def handle_info(
        {:EXIT, client, reason},
        %{client: client} = state
      ) do
    config = current_config(state)

    reason =
      case reason do
        {:shutdown, shutdown_reason} -> shutdown_reason
        _ -> reason
      end

    {reconnect, state} =
      if reason == :normal do
        {:immediate, state}
      else
        rotate_config(state)
      end

    {reconnect_timeout, state} =
      if reconnect == :backoff do
        {reconnect_timeout, backoff} = state.backoff
        state = %{state | backoff: :backoff.fail(backoff)}
        {reconnect_timeout, state}
      else
        {0, state}
      end

    Logger.warning(
      "#{__MODULE__} client exited url=#{Config.to_safe_string(config)} reason=#{inspect(reason)} reconnect_after=#{reconnect_timeout}"
    )

    Process.send_after(self(), :reconnect, reconnect_timeout)

    {handler, handler_state} = state.handler
    {:ok, handler_state} = handler.handle_disconnected(reason, handler_state)

    state = %{
      state
      | client: nil,
        status: :not_connected,
        handler: {handler, handler_state}
    }

    {:noreply, state}
  end

  def handle_info({:EXIT, port, :normal}, state) when is_port(port) do
    # not clear in what situations we receive these, but they don't appear to
    # crash the :emqtt connection, so we ignore them.
    {:noreply, state}
  end

  def handle_info(unknown, state) do
    config = current_config(state)

    Logger.warning(
      "#{__MODULE__} ignoring unexpected message url=#{Config.to_safe_string(config)} message=#{inspect(unknown)}"
    )

    {:noreply, state}
  end

  @impl GenServer
  def terminate(reason, state) do
    if state.status == :connected do
      {handler, handler_state} = state.handler
      {:ok, _} = handler.handle_disconnected(reason, handler_state)
      :emqtt.disconnect(state.client)
    end

    :ok
  catch
    :exit, _ -> :ok
  end

  @spec init_configs(config() | Enumerable.t(config())) :: [config()]
  defp init_configs(configs) do
    configs
    |> List.wrap()
    |> Enum.map(fn config ->
      case config do
        %Config{} -> config
        url when is_binary(url) -> Config.from_url(url)
      end
    end)
  end

  @spec init_handler(handler() | nil) :: {module(), term()}
  defp init_handler(nil) do
    init_handler(DefaultHandler)
  end

  defp init_handler(handler) do
    {module, init_args} =
      case handler do
        {module, init_args} when is_atom(module) ->
          {module, init_args}

        module when is_atom(module) ->
          {module, []}
      end

    {:ok, handler_state} = module.init(init_args)
    {module, handler_state}
  end

  @spec init_backoff(backoff()) :: {non_neg_integer(), :backoff.backoff()}
  defp init_backoff({start, max}) do
    {start, :backoff.init(start, max)}
  end

  defp init_backoff({start, max, type}) do
    backoff = :backoff.init(start, max)
    backoff = :backoff.type(backoff, type)
    {start, backoff}
  end

  defp init_backoff(start) when is_number(start) do
    {start, :backoff.init(start, start)}
  end

  @spec handle_connect_response({:ok, term()} | {:error, term()}, state()) :: {:noreply, state()}
  defp handle_connect_response({:ok, _properties}, state) do
    config = current_config(state)
    Logger.info("#{__MODULE__} connected url=#{Config.to_safe_string(config)}")
    state = %{state | status: :connected}
    {handler, handler_state} = state.handler
    {:ok, topics, handler_state} = handler.handle_connected(handler_state)

    {_, backoff} = state.backoff

    state = %{state | backoff: :backoff.succeed(backoff), handler: {handler, handler_state}}

    if Enum.empty?(topics) do
      {:noreply, state}
    else
      subscriptions =
        for topic <- topics do
          {topic, []}
        end

      case :emqtt.subscribe(state.client, subscriptions) do
        {:ok, _, _} ->
          {:noreply, state}

        {:error, reason} ->
          handle_connect_response({:error, reason}, state)
      end
    end
  end

  defp handle_connect_response({:error, reason}, state) do
    _ = :emqtt.disconnect(state.client)
    Process.exit(state.client, {:shutdown, reason})
    {:noreply, state}
  catch
    :exit, _ -> {:noreply, state}
  end

  @spec current_config(state()) :: Config.t()
  defp current_config(state) do
    Enum.at(state.configs, state.config_index)
  end

  @spec rotate_config(state()) :: {:immediate | :backoff, state()}
  defp rotate_config(state) do
    next_index = rem(state.config_index + 1, length(state.configs))

    reconnect =
      if next_index == 0 do
        :backoff
      else
        :immediate
      end

    state = %{state | config_index: next_index}
    {reconnect, state}
  end

  @spec call_publish_error_reply(term(), Message.t()) :: :ok | {:error, term()}
  defp call_publish_error_reply(error, message)

  defp call_publish_error_reply(_, %Message{qos: 0}) do
    :ok
  end

  defp call_publish_error_reply(:noproc, _message) do
    {:error, :not_connected}
  end

  defp call_publish_error_reply({:shutdown, error}, message) do
    call_publish_error_reply(error, message)
  end

  defp call_publish_error_reply(error, _message) do
    {:error, error}
  end
end
