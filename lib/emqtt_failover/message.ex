defmodule EmqttFailover.Message do
  @moduledoc """
  A message to/from an MQTT topic.

  ## QOS (Quality of Server)
  - 0: at most once
  - 1: at least once
  - 2: exactly once

  ## Retain?
  Each topic can have one retained message, which is sent to clients when they subscribe to the topic.

  If `retain?` is true when sending the message, the message will be the new retained message on the topic.
  If `retain?` is true when receiving the message, the message was the retained message on the topic.

  Note: once the client has subscribed to the topic, new messages send with the
  `retain?` flag will not be received with the `retain?` flag set.
  """
  defstruct [:topic, :payload, qos: 0, retain?: false]

  @type topic :: binary
  @type payload :: binary
  @type t :: %__MODULE__{
          topic: topic(),
          payload: payload(),
          qos: 0 | 1 | 2,
          retain?: boolean()
        }
end
