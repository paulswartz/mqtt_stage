defmodule EmqttFailover.ConnectionHandler.Default do
  @moduledoc """
  Default EmqttFailover.Connection which does nothing.
  """
  use EmqttFailover.ConnectionHandler

  @impl EmqttFailover.ConnectionHandler
  def init(_opts), do: {:ok, []}
end
