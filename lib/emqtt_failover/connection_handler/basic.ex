defmodule EmqttFailover.ConnectionHandler.Parent do
  @moduledoc """
  `EmqttFailover.ConnectionHandler` which sends messages back to a parent
  process.

  Messages:
  - `{:connected, pid}` on connection, where `pid` is the `EmqttFailover.Connection` process
  - `{:disconnected, pid, reason}` on disconnection
  - `{:message, pid, message}` on receipt of a message

  Options:
  - `parent` (required): parent process to send messages to
  - `topics` (optional): list of topics to subscribe to on connection.
  """
  use EmqttFailover.ConnectionHandler

  defstruct [:parent, :topics]

  @impl EmqttFailover.ConnectionHandler
  def init(opts) do
    state = %__MODULE__{
      parent: Keyword.fetch!(opts, :parent),
      topics: Keyword.get(opts, :topics, [])
    }

    {:ok, state}
  end

  @impl EmqttFailover.ConnectionHandler
  def handle_connected(state) do
    send(state.parent, {:connected, self()})
    {:ok, state.topics, state}
  end

  @impl EmqttFailover.ConnectionHandler
  def handle_disconnected(reason, state) do
    send(state.parent, {:disconnected, self(), reason})
    {:ok, state}
  end

  @impl EmqttFailover.ConnectionHandler
  def handle_message(message, state) do
    send(state.parent, {:message, self(), message})
    {:ok, state}
  end
end
