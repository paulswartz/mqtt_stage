defmodule EmqttFailover.ConnectionHandler do
  @moduledoc """
  Behavior for handling messages in an EmqttFailover.Connection.
  """

  @type state :: term()

  @callback init(term) :: {:ok, state}

  @callback handle_connected(state) :: {:ok, Enumerable.t(EmqttFailover.Message.topic()), state}

  @callback handle_disconnected(reason :: term, state) :: {:ok, state}

  @callback handle_message(EmqttFailover.Message.t(), state) :: {:ok, state}

  defmacro __using__(_) do
    quote do
      @behaviour EmqttFailover.ConnectionHandler

      def handle_connected(state) do
        {:ok, [], state}
      end

      def handle_disconnected(_reason, state) do
        {:ok, state}
      end

      def handle_message(_message, state) do
        {:ok, state}
      end

      defoverridable handle_connected: 1, handle_disconnected: 2, handle_message: 2
    end
  end
end
