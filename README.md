# EmqttFailover

[![pipeline status](https://gitlab.com/paulswartz/emqtt_failover/badges/main/pipeline.svg)](https://gitlab.com/paulswartz/emqtt_failover/commits/main) [![coverage report](https://gitlab.com/paulswartz/emqtt_failover/badges/main/coverage.svg)](https://gitlab.com/paulswartz/emqtt_failover/commits/main)

Wrapper around [emqtt][emqtt] which provides support for
failing over between brokers.


## Examples

To publish messages to a topic:

``` elixir
iex> {:ok, pid} = EmqttFailover.Connection.start_link(
...    configs: [
...      "mqtts://test.mosquitto.org:8886", 
...      "mqtt://test.mosquitto.org:1883"])
iex> EmqttFailover.Connection.publish(pid, 
...    %EmqttFailover.Message{topic: "topic", payload: "payload", qos: 1})
:ok
```

To subscribe to topics and react to the messages, implement the `EmqttFailover.ConnectionHandler` behaviour:

``` elixir
defmodule Handler do
  @behaviour EmqttFailover.ConnectionHandler
  
  def init(_opts) do
    {:ok, []}
  end
  
  def handle_connected(state) do
    {:ok, ["topic/a", "topic/b"], state}
  end
  
  def handle_message(%EmqttFailover.Message{} = _message, state) do
    # react to the message
    {:ok, state}
  end
end

iex> {:ok, pid} = EmqttFailover.Connection.start_link(
...    configs: ["mqtts://test.mosquitto.org:8886"],
...    handler: Handler)
```

## Installation

The package can be installed by adding `emqtt_failover` to your list of dependencies in `mix.exs`:

``` elixir
def deps do
  [
    {:emqtt_failover, "~> 0.3"}
  ]
end
```

## Documentation

View the full API documentation on [Hexdocs][hexdocs] or [GitLab Pages](glp).

## Local development

```sh
> docker compose start
> mix test  # or mix coveralls.html
> mix format
> mix credo --strict
> mix dialyzer --format dialyzer
> mix docs
```

## License
Code from [emqtt][emqtt] is (c) EMQ Technologies Co, Ltd. under the Apache License, Version 2.0.

Other code is released under the [MIT license][LICENSE]. 

[emqtt]: https://github.com/emqx/emqtt
[hexdocs]: https://hexdocs.pm/emqtt_failover
[glp]: https://paulswartz.gitlab.io/emqtt_failover/readme.html
[LICENSE]: ./LICENSE
