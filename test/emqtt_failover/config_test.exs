defmodule EmqttFailover.ConfigTest do
  @moduledoc false
  use ExUnit.Case, async: true
  alias EmqttFailover.Config

  describe "from_url/1" do
    test "parses simple URL into a config" do
      config = Config.from_url("mqtt://username:password@hostname:1234")

      assert %Config{
               host: "hostname",
               port: 1234,
               username: "username",
               password: "password",
               ssl?: false
             } == config
    end

    test "defaults to port 1883" do
      assert %Config{port: 1883} = Config.from_url("mqtt://hostname")
    end

    test "parses mqtts:// URLs" do
      assert %Config{host: "hostname", ssl?: true, port: 8883} =
               Config.from_url("mqtts://hostname")
    end

    test "parses mqtt+ssl:// URLs" do
      assert %Config{host: "hostname", ssl?: true, port: 8883} =
               Config.from_url("mqtt+ssl://hostname")
    end

    test "parses a username without a password" do
      assert %Config{username: "user", password: nil} = Config.from_url("mqtt://user@hostname")
    end

    test "raises an error for an unknown scheme" do
      error =
        assert_raise(ArgumentError, fn ->
          Config.from_url("https://hostname")
        end)

      assert error.message =~ "https"
    end
  end
end
