defmodule EmqttFailover.ConnectionTest do
  @moduledoc false
  use ExUnit.Case

  alias EmqttFailover.Config
  alias EmqttFailover.Connection
  alias EmqttFailover.ConnectionHandler.Parent
  alias EmqttFailover.Message

  @moduletag :capture_log

  @activemq_hostname Application.compile_env(:emqtt_failover, :test_hostname)
  @test_url "mqtt://system:manager@#{@activemq_hostname}"
  @test_config Config.from_url(@test_url)
  @receive_timeout 2_000

  describe "start_link/1" do
    test "can start a connection to an MQTT server" do
      pid = start_supervised!({Connection, @test_config})
      assert {:connected, @test_config} = await_connected(pid)
    end

    test "can start a connection to an MQTT server given a URL" do
      pid = start_supervised!({Connection, @test_url})
      assert {:connected, @test_config} = await_connected(pid)
    end

    test "can accept a passed-in client_id" do
      client_id = EmqttFailover.client_id(prefix: "emqtt-f-testing")
      old_level = Logger.level()

      on_exit(fn ->
        Logger.configure(level: old_level)
      end)

      log =
        ExUnit.CaptureLog.capture_log([level: :debug], fn ->
          Logger.configure(level: :debug)
          pid = start_supervised!({Connection, configs: @test_config, client_id: client_id})
          await_connected(pid)
          Logger.configure(level: old_level)
        end)

      assert log =~ "clientid: \"#{client_id}\""
    end

    @tag :online
    test "can connect to a server authenticated with a Let's Encrypt cert" do
      url = "mqtts://test.mosquitto.org:8886"
      pid = start_supervised!({Connection, configs: url})
      assert {:connected, _} = await_connected(pid)
    end

    @tag :online
    test "rejects connections to a server with an invalid certificate" do
      url = "mqtts://test.mosquitto.org:8883"

      log =
        ExUnit.CaptureLog.capture_log([level: :warning], fn ->
          pid = start_supervised!({Connection, configs: url})
          {result, _} = await_connected(pid)
          refute result == :connected
        end)

      assert log =~ "Unknown CA"
    end

    @tag :online
    test "accepts connections to a server with a provided certificate" do
      ssl_opts =
        "test.mosquitto.org"
        |> Config.default_ssl_opts()
        |> Keyword.delete(:cacerts)
        |> Keyword.put(:cacertfile, "test/fixtures/mosquitto.org.crt")

      config = Config.from_url("mqtts://test.mosquitto.org:8883", ssl_opts: ssl_opts)
      pid = start_supervised!({Connection, configs: config})
      assert {:connected, ^config} = await_connected(pid)
    end

    test "can connect to an MQTT server with a secondary config without waiting for a backoff" do
      configs = [
        Config.from_url("mqtt://invalid:username@#{@activemq_hostname}"),
        @test_config
      ]

      pid = start_supervised!({Connection, configs: configs})

      assert {:connected, @test_config} = await_connected(pid)
    end

    test "reconnects immediately when the connection is terminated" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}

      {:ok, pid} = Connection.start_link(configs: @test_config, handler: handler)
      await_connected(pid)

      Connection.publish(pid, %Message{
        topic: topic,
        payload: payload,
        qos: 1,
        retain?: true
      })

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout

      # disconnect the connect directly
      port = Connection.port(pid)
      Port.close(port)

      assert_receive {:disconnected, ^pid, :normal}

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload, retain?: true}},
                     @receive_timeout
    end

    test "can be named" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}
      name = __MODULE__
      pid = start_supervised!({Connection, configs: @test_config, handler: handler, name: name})

      await_connected(name)

      Connection.publish(name, %Message{topic: topic, payload: payload})

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout
    end

    test "backs off exponentially by default" do
      # chooses a random port
      url = "mqtt://#{@activemq_hostname}:0"

      log =
        ExUnit.CaptureLog.capture_log([level: :warning], fn ->
          start_supervised!({Connection, configs: url, backoff: {10, 1_000}})
          Process.sleep(200)
        end)

      refute log =~ "reconnect_after=0"
      assert log =~ "reconnect_after=10"
      assert log =~ "reconnect_after=20"
      assert log =~ "reconnect_after=40"
    end

    test "with multiple configs, backs off exponentially after trying all configs" do
      # chooses a random port
      configs = [
        Config.from_url("mqtt://#{@activemq_hostname}:0"),
        Config.from_url("mqtts://#{@activemq_hostname}:0")
      ]

      log =
        ExUnit.CaptureLog.capture_log([level: :warning], fn ->
          start_supervised!({Connection, configs: configs, backoff: {10, 1_000}})
          Process.sleep(200)
        end)

      [mqtt_0, mqtts_0, mqtt_1, mqtts_1 | _] =
        log
        |> String.split("\n")
        |> Enum.filter(&(&1 =~ "reconnect_after="))

      assert mqtt_0 =~ "reconnect_after=0"
      assert mqtts_0 =~ "reconnect_after=10"
      assert mqtt_1 =~ "reconnect_after=0"
      assert mqtts_1 =~ "reconnect_after=20"
    end

    test "logs an unknown message, does not crash" do
      message = {:unknown, make_ref()}

      log =
        ExUnit.CaptureLog.capture_log([level: :warning], fn ->
          {:ok, pid} = Connection.start_link(configs: @test_config)
          send(pid, message)
          await_connected(pid)
          assert Process.alive?(pid)
          Connection.stop(pid)
        end)

      assert log =~ inspect(message)
    end
  end

  describe "publish/2" do
    test "can publish and subscribe to a topic (qos: 1)" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}
      pid = start_supervised!({Connection, configs: @test_config, handler: handler})
      await_connected(pid)
      Connection.publish(pid, %Message{topic: topic, payload: payload, qos: 1})

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout
    end

    test "can publish to a topic (qos: 0)" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}
      pid = start_supervised!({Connection, configs: @test_config, handler: handler})
      await_connected(pid)
      Connection.publish(pid, %Message{topic: topic, payload: payload, qos: 0})

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout
    end

    test "can receive a retained message" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}

      {:ok, publish_pid} = Connection.start_link(configs: @test_config)
      await_connected(publish_pid)

      Connection.publish(publish_pid, %Message{
        topic: topic,
        payload: payload,
        qos: 1,
        retain?: true
      })

      {:ok, listen_pid} = Connection.start_link(configs: @test_config, handler: handler)

      assert_receive {:message, ^listen_pid,
                      %Message{topic: ^topic, payload: ^payload, retain?: true}},
                     @receive_timeout
    end

    test "responds with an error if the host is offline (qos: 1)" do
      # chooses a random port
      url = "mqtt://#{@activemq_hostname}:0"
      {:ok, pid} = Connection.start_link(configs: url)

      assert {:error, :not_connected} =
               Connection.publish(pid, %Message{
                 topic: "topic",
                 payload: "payload",
                 qos: 1
               })

      assert Process.alive?(pid)
    end

    test "responds with an error if the connection does not authenticate (qos: 1)" do
      # chooses a random port
      url = "mqtt://invalid@#{@activemq_hostname}/"
      {:ok, pid} = Connection.start_link(configs: url)

      assert {:error, reason} =
               Connection.publish(pid, %Message{
                 topic: "topic",
                 payload: "payload",
                 qos: 1
               })

      assert reason in [:not_connected, :unauthorized_client]
      assert Process.alive?(pid)
    end

    test "responds with :ok if the connection is not connected (qos: 0)" do
      # chooses a random port
      url = "mqtt://localhost:0"
      {:ok, pid} = Connection.start_link(configs: url)

      assert :ok =
               Connection.publish(pid, %Message{
                 topic: "topic",
                 payload: "payload",
                 qos: 0
               })

      assert Process.alive?(pid)
    end

    test "publishing a message immediately without connecting does not crash" do
      # create a TCP listener to accept the connection, but not do anything with it
      {:ok, listener} = :gen_tcp.listen(0, [:binary])
      {:ok, port} = :inet.port(listener)
      url = "mqtt://localhost:#{port}"

      pid = start_supervised!({Connection, configs: url})

      assert {:error, :not_connected} =
               Connection.publish(pid, %Message{
                 topic: unique_topic(),
                 payload: unique_payload(),
                 qos: 1
               })
    end
  end

  describe "publish_async/2" do
    test "can publish and subscribe to a topic (qos: 0)" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}
      pid = start_supervised!({Connection, configs: @test_config, handler: handler})
      await_connected(pid)
      :ok = Connection.publish_async(pid, %Message{topic: topic, payload: payload, qos: 0})

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout
    end

    test "can publish and subscribe to a topic (qos: 1, not recommended)" do
      topic = unique_topic()
      payload = unique_payload()
      handler = {Parent, parent: self(), topics: [topic]}
      pid = start_supervised!({Connection, configs: @test_config, handler: handler})
      await_connected(pid)
      :ok = Connection.publish_async(pid, %Message{topic: topic, payload: payload, qos: 1})

      assert_receive {:message, ^pid, %Message{topic: ^topic, payload: ^payload}},
                     @receive_timeout
    end

    test "responds with :ok if the connection is not connected (qos: 0)" do
      # chooses a random port
      url = "mqtt://localhost:0"
      {:ok, pid} = Connection.start_link(configs: url)

      assert :ok =
               Connection.publish_async(pid, %Message{
                 topic: "topic",
                 payload: "payload",
                 qos: 0
               })

      assert Process.alive?(pid)
    end
  end

  describe "stop/1" do
    test "can stop a connection" do
      {:ok, pid} =
        Connection.start_link(
          configs: [@test_config],
          handler: {Parent, parent: self()}
        )

      {:connected, @test_config} = await_connected(pid)
      port = Connection.port(pid)
      assert :ok = Connection.stop(pid)
      assert_received {:disconnected, ^pid, :normal}
      refute Port.info(port)
    end
  end

  defp await_connected(pid, timeout \\ 2_000) do
    case Connection.status(pid) do
      {:connected, _} = status ->
        status

      other ->
        if timeout do
          Process.send_after(self(), :timeout, timeout)
        end

        # if we received a timeout message, respond with the error; otherwise loop
        receive do
          :timeout -> other
        after
          10 -> await_connected(pid, nil)
        end
    end
  end

  defp unique_topic, do: "emqtt_failover/#{System.unique_integer()}"

  defp unique_payload do
    number = System.unique_integer()
    <<number::integer-128>>
  end
end
