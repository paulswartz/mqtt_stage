defmodule EmqttFailoverTest do
  @moduledoc false
  use ExUnit.Case, async: true

  describe "client_id" do
    test "defaults to using 'emqtt-f' as the prefix and the inet hostname" do
      {:ok, hostname} = :inet.gethostname()
      client_id = EmqttFailover.client_id()
      assert String.starts_with?(client_id, "emqtt-f-#{hostname}-")
    end

    test "are unique between runs" do
      refute EmqttFailover.client_id() == EmqttFailover.client_id()
    end

    test "can set prefix and hostname" do
      client_id = EmqttFailover.client_id(prefix: "client", hostname: "host")
      assert String.starts_with?(client_id, "client-host-")
    end
  end
end
