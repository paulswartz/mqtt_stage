defmodule EmqttFailover.MixProject do
  use Mix.Project

  def project do
    [
      app: :emqtt_failover,
      name: "EmqttFailover",
      description: "Wrapper around :emqtt to provide support for failing over between brokers",
      version: "0.3.1",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.cobertura": :test,
        "coveralls.html": :test
      ],
      dialyzer: dialyzer(),
      deps: deps(),
      docs: [
        main: "readme",
        extras: ["README.md", "LICENSE"]
      ],
      package: [
        licenses: ["MIT"],
        maintainers: ["Paul Swartz <paul@paulswartz.net"],
        files: ~w(README.md CHANGELOG.md LICENSE include src lib mix.exs),
        links: %{
          "GitLab" => "https://gitlab.com/paulswartz/emqtt_failover"
        },
        source_url: "https://gitlab.com/paulswartz/emqtt_failover",
        homepage_url: "https://gitlab.com/paulswartz/emqtt_failover"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ssl, :crypto]
    ]
  end

  def aliases do
    [
      ci: [
        "format",
        "credo --strict",
        "dialyzer --format dialyzer",
        fn _ -> Mix.env(:test) end,
        "test"
      ]
    ]
  end

  defp dialyzer do
    defaults = [
      ignore_warnings: "dialyzer.ignore-warnings"
    ]

    if core_path = System.get_env("DIALYZER_CORE_PATH") do
      [plt_core_path: core_path] ++ defaults
    else
      defaults
    end
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:backoff, "~> 1.1.6"},
      {:benchee, "~> 1.1", only: :dev},
      {:credo, "~> 1.7", only: [:dev], runtime: false},
      {:dialyxir, "~> 1.4", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.17.1", only: [:test], runtime: false},
      {:ex_doc, "~> 0.30.6", only: [:dev], runtime: false},
      {:junit_formatter, "~> 3.3", only: [:test]}
    ]
  end
end
